# Note:
This project was intially a partner project through the university, but the repository it was hosted on was deleted. This is an upload to my private GitLab, so previous commits are lost and the automatic pipeline tests through Git are not functional. 

# Simulation of Ecosystem on an Island

The goal of this project was to create classes that together simulates life on a virtual map,
Rossumøya, with different landscape types and two general types of faunas. 

Details about the project can be found in: 
[project_description](INF200_BioSim.pdf)


## Landscapes

There are 4 landscape types on this island, different in terms of fodder available and passability.
The map is 21 by 22 (height x width) and shows a landmass surrounded by water. 

![RossumIsland](docs/_static/Rossum_island.png)


The landscape types: 

1. Water 

```
Impassable, no animal can start off or migrate to a water-cell
```

2. Lowland
   
```
Plentiful amount of fodder for herbivores
```

3. Highland
   
```
Less fodder available than in Lowland
```

4. Desert

```
No fodder available for herbivores
```


## Animal Inhabitants
There are two types of animals on Rossumøya and they live together in a form of parasittic 
symbiosis; One species lives without interfering with the other, while the other preys
on the first species. The two species are:

1. Herbivores

* Eats grass, animals eat in random order
* Migrates randomly across the landscapes

2. Carnivores

* Eats herbivores depending on fitness; carnivore with largest fitness eats first
* The herbivores with lowest fitness are killed first
* Migrates randomly across the landscapes


# Key files

Source code package
```
src/biosim/

> animals
> landscape
> island
> simulation
```

Simulation examples: 
```
examples/

> check_sim
> mono_hc
> mono_ho
> sample_sim
> migration_checkboard
```

Tests
```
tests/

> test_animals
> test_island
> test_landscape
> test_biosim_interface
```

# Usage
Import
```
import biosim
```

Make an instance of an Animal (in this case, a herbivore)
```
herb = animals.Animals({"species": "Herbivore", "age": 5, "weight": 21.5})
```

Make an instance of a landscape type, with 2 animals in it
```
some_animals = [{"species": "Herbivore", "age": 5, "weight": 21.5},
                {"species": "Carnivore", "age": 1, "weight": 3.2}]
                
lowland_cell = landscape.Lowland(some_animals)
```

Make an instance of an island
```
Skagerak_map = """WWW\nWHW\nWWW"""
Skagerak_pop = [{"species": "Herbivore", "age": 5, "weight": 21.5},
                {"species": "Carnivore", "age": 1, "weight": 3.2}]
                
Skagerak = island.Island(Skagerak_map, Skagerak_pop)
```

Make a biosim-instance for our island
```
Skagerak_map = """WWW\nWHW\nWWW"""
Skagerak_pop = [{"species": "Herbivore", "age": 5, "weight": 21.5},
                {"species": "Carnivore", "age": 1, "weight": 3.2}]

sim = simulation.BioSim(Skagerak_map, Skagerak_pop, seed=42)
```

# Comments about project

As previously mentioned, we divided our source code into 4 files, building upon each other in this
order: animals > landscape > island > simulation. From the parent-class Animals, two sub-classes 
were made (Herbivore, Carnivore), and from the parent class Landscape, four sub-classes were made, 
one for each of the 4 terrain types. Each module in the biosim-package only imports from the 
next module below in the hierarchy (e.g. Island imports only from Landscape, not Animals). 

## Additional functionality

In the examples folder, an additional example was added - migration_checkboard, showing whether
an animal population migrates in a checkerboard-pattern when all animals are guaranteed to migrate
every year. A checherboard test was also added into the test_island file. 

In the documentation, LaTeX, tables and automodules were used, to properly format functions and
represent variables and classes. 

In the source code, we added a method to assign a random, non-water-cell location to an animal or 
animal population, given that their location = None. We also added a small test to check the 
validity of any given location in the initial or added population, silently ignoring any animals
that is given a location not in the map, or a location that belongs to a water cell. See 
the method assign_location and add_population in the class Landscape. 

## Room for improvement

We did not have much time to look into optimization of the code, but we did write consciously as
to avoid unnecessary if-tests and loops in the code. If we had better time, we would also have 
implemented a GUI, and added an option / buttons to increase or decrease the speed of the produced 
video. 
