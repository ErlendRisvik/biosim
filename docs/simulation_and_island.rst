
Simulation and Island
=====================

|

Examples
----------

Create an instance of Island

.. code-block:: python
    :linenos:

    from biosim.island import Island
    import textwrap

    geogr = """\
               WWWW
               WHHW
               WLDW
               WWWW"""

    geogr = textwrap.dedent(geogr)

    ini_herbs = [{'loc': (2, 7),
                  'pop': [{'species': 'Herbivore','age': 5,'weight': 20}
                          for _ in range(100)]}]
    ini_carns = [{'loc': (2, 7),
                  'pop': [{'species': 'Carnivore','age': 5,'weight': 20}
                          for _ in range(25)]}]

    our_island = Island(geography = geogr, ini_pop = ini_herbs + ini_carns)
..

|

Run 3 rounds of seasons, then add more animals

.. code-block:: python
    :linenos:

     for _ in range(3):
        our_island.run_one_season()

     new_animals = [{'loc': (5, 5),
                     'pop': [{"species": "Herbivore", "age": 2, "weight": 10}]},
                    {'loc': (2, 6),
                     'pop': [{"species": "Carnivore", "age": 4, "weight": 21}]}]

     our_island.add_population(new_animals)  #Add more animals
..
|
|

Create an instance of the BioSim class, and run a simulation

.. code-block:: python
    :linenos:

    from biosim.simulation import BioSim
    import textwrap

    geogr = """\
               WWWW
               WHHW
               WLDW
               WWWW"""

    geogr = textwrap.dedent(geogr)

    ini_herbs = [{'loc': (2, 7),
                  'pop': [{'species': 'Herbivore','age': 5,'weight': 20}
                          for _ in range(100)]}]

    sim = BioSim(geogr, ini_herbs + ini_carns, seed=1,
                 hist_specs={'fitness': {'max': 1.0, 'delta': 0.05},
                             'age': {'max': 60.0, 'delta': 2},
                             'weight': {'max': 60, 'delta': 2}},
                 img_dir='results',
                 img_base='sample')

    sim.simulate(50)   #simulate 50 years
..

|

Add more animals, and continue the simulation

.. code-block:: python
    :linenos:

    new_carns = [{'loc': (2, 7),
                  'pop': [{'species': 'Carnivore','age': 5,'weight': 20}
                          for _ in range(25)]}]

    sim.add_population(population=new_carns)   #add new animals
    sim.simulate(num_years=100)                #simulate another 100 years
..


|
|
|
|


The simulation module
---------------------
.. automodule:: biosim.simulation
   :members:

|
|
|
|

The island module
-----------------
.. automodule:: biosim.island
   :members:
