
Landscape and Animals
======================

|

Examples
----------

Create an instance of a Herbivore

.. code-block:: python
    :linenos:

    import biosim

    a_herb = animals.Herbivore(values = {"species": "Herbivore",
                                         "age": 2, "weight": 4.5} )

    a_herb.fitness  #get it's fitness
    a_herb.death()  #run the method "death"
    a_herb.alive    #check if the herbivore is still alive
..

You can also use the Animal directly

.. code-block:: python
    :linenos:

    import biosim

    a_herb = animals.Animals(values = {"species": "Herbivore",
                                       "age": 2, "weight": 4.5})
..

Create an instance of the landscape type Lowland

.. code-block:: python
   :linenos:

   import biosim

   some_animals = [{"species": "Herbivore", "age": 2, "weight": 4.5},
                   {"species": "Carnivore", "age": 1, "weight": 2.0},
                   {"species": "Herbivore", "age": 5, "weight": 21.8}]

   biom_L = landscape.Lowland(animal_population = some_animals)

   biom_L.is_land               #Check if this biome is land or water
   biom_L.herbivores            #get the list of registered herbivores
   biom_L.give_birth_in_cell()  #run method, animals might multiply
..

|
|
|


The animals module
-------------------

**Weight & age**


The weight for animal population is normally distributed, with :math:`w - N(w_{birth}, \sigma_{birth})`.
At birth, age, :math:`a = 0`. Changes that affect weight are:

* Yearly loss = :math:`\eta w`
* Weight increase due to food, :math:`F`, eaten = :math:`\beta F`
* Weight loss due to birthing = :math:`\xi  w_{baby}`

This means:

.. math::
   w_{k+1} = w_k + \beta F - (\eta*w + \xi  w_{baby})

For Carnivores, who eat herbivores, F is the sum of the herbivores weights that were
eaten, e.g:

.. math::
   F \approx \sum_{i = 1}^{herbsTot}{ (w_{herb}) }

**Fitness - Migration, Birth, Death**


**Fitness**

Fitness, which is used to describe the overall condition of the animal, is calculated by
the formula:

.. math::
   \Phi = \begin{cases}0 & w \le 0
    \\ q^+(a, a_{\frac{1}{2}}, \phi_{age}) \times
    q^-(w, w_{\frac{1}{2}}, \phi_{weight}) & else \\
    \end{cases}

Where

.. math::
   q^{\pm}(x, x_{\frac{1}{2}}, \phi) = \frac{1}{1 + e^{\pm \phi(x - x_{\frac{1}{2}})}}


**Migration**

Fitness affects migration. An animal will try and move with probability = :math:`\mu \Phi`.
If the animal decides to move, a random direction will be picked (north, south, east of west),
and if the chosen new land cell is not Water, the animal migrates.

**Birth**

Whether an animal can give birth in the specific year, is dependent on their fitness,
species-specific constants, and the number of same-species animals (:math:`N`) in the cell.
The probability of giving birth is:

.. math::
   \begin{cases} 0 & N = 1 \\
   0 & \Phi = 0 \\
   0 & w < \zeta \times (w_{birth} + \sigma_{birth}) \\
   0 & w < \xi \times w_{birth} \\
   min(1, \gamma \times \Phi \times (N-1)) & else \\
   \end{cases}

Which means, there must be more than just the self-animal in the cell. Its weight must be
large enough. And randomness must allow the birth to happen.

**Death**

Probability of death is dependent on fitness and a species-specific constant. The probability
is calculated as follows:

.. math::
   \begin{cases} 0 & w = 0 \\
   \omega \times (1 - \Phi) & else \\
   \end{cases}

Death of Herbivores are also affected by Carnivores and their fitness, which try to kill
Herbivores in order of fitness. The probability of a carnivore killing a herbivore is:

.. math::
   \begin{cases} \\
   0 & \Phi_{carn} \le \Phi_{herb} \\
   \frac{\Phi_{carn} - \Phi_{herb}}{\Delta \Phi_{max}} & (\Phi_{carn} - \Phi_{herb}) \in
       \langle 0, \Delta \Phi_{max} \rangle \\
   1 & else \\
   \end{cases}

|
|

**The classes & modules**


|

1. Animals
2. Herbivore
3. Carnivore

|

.. list-table:: Description of greek letters

   * - Greek letter
     - Name
     - Type
     - Description
   * - :math:`w_{birth}`
     - w_birth
     - float
     - Expected weight of the normally distributed population
   * - :math:`w_{\frac{1}{2}}`
     - w_half
     - float
     - Related to change in fitness
   * - :math:`a_{\frac{1}{2}}`
     - a_half
     - float
     - Related to change in fitness
   * - :math:`\sigma_{birth}`
     - Sigma_birth
     - float
     - Standard deviation of the normally distributed population
   * - :math:`\beta`
     - Beta
     - float
     - Multiplied with yearly eaten fodder = weight change due to eating
   * - :math:`\eta`
     - Eta
     - float
     - Multiplied with the animals current weight = weight reduction that year
   * - :math:`\Phi_{age}`
     - Phi_age
     - float
     - Related to change in fitness
   * - :math:`\Phi_{weight}`
     - Phi_weight
     - float
     - Related to change in fitness
   * - :math:`\mu`
     - Mu
     - float
     - Multiplied with fitness, yielding the probability of migration
   * - :math:`\gamma`
     - Gamma
     - float
     - Multiplied with fitness and (N-1), to get prob of giving birth
   * - :math:`\zeta`
     - Zeta
     - float
     - related to threshold for minimum weight accepted for the birthing animal
   * - :math:`\xi`
     - Xi
     - float
     - Multiplied with baby weight = weight the mother loses by giving birth
   * - :math:`\omega`
     - Omega
     - float
     - Multiplied with (1-fitness) = probability of death
   * - :math:`\Delta \Phi_{max}`
     - DeltaPhiMax
     - float
     - Related to carnivores killing herbivores
   * - :math:`F`
     - F
     - float
     - Amount of fodder the animal wishes to eat each year
..


.. automodule:: biosim.animals
   :members:


|
|
|
|

The landscape module
----------------------

|
|

**The classes & modules**


|

1. Landscape
2. Lowland
3. Highland
4. Desert
5. Water

|

.. automodule:: biosim.landscape
   :members:


|
|
|

