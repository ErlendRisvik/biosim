.. Rossum_Biosim_T12 documentation master file, created by
   sphinx-quickstart on Sun Jun  6 17:37:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Rossumøya Island Biosim's documentation!
=============================================

This is a simulation whose goal is to simulate

* An island
* With different landscape types
* Containing migrating animal populations

The simulation module imports from the island module, and illustrates how the life cycle of animals
on an imaginary island proceeds, given a map that the user might input. For illustrative purposes,
the island in question is Rossumøya, whose 22 by 21 map show an island surrounded by water.

.. figure:: _static/Rossum_island.png
   :align:  center

   FIGURE 1: The map of the island Rossumøya has 4 types of landscapes

Following this, the landscape modules imports from the animals module. Together creating an island
with landscape types of different attributes, containing animals.

.. toctree::
   :maxdepth: 3

   simulation_and_island
   landscape_and_animals
   Output_examples


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
