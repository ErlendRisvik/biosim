# -*- coding: utf-8 -*-

"""
The material in this file is licensed under the BSD 3-clause license
https://opensource.org/licenses/BSD-3-Clause
Copyright (c) 2021 Hans Ekkehard Plesser / NMBU

Island with single jungle cell, first herbivores only, later carnivores.
Mono map
"""


__author__ = 'Hans Ekkehard Plesser, NMBU'


import textwrap
from biosim.simulation import BioSim

geogr = """\
           WWW
           WLW
           WWW"""
geogr = textwrap.dedent(geogr)

ini_herbs = [{'loc': (2, 2),
              'pop': [{'species': 'Herbivore',
                       'age': 5,
                       'weight': 20}
                      for _ in range(50)]}]

for seed in range(100, 103):
    sim = BioSim(geogr, ini_herbs, seed=seed,
                 img_dir='results', img_base=f'mono_ho_{seed:05d}', img_years=300)
    sim.simulate(301)
