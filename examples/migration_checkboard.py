# -*- coding: utf-8 -*-

import textwrap
from biosim.simulation import BioSim


geogr = """\
           WWWWWWW
           WLLLLLW
           WLLLLLW
           WLLLLLW
           WLLLLLW
           WLLLLLW
           WWWWWWW"""
geogr = textwrap.dedent(geogr)
ini_herbs = [{'loc': (4, 4),
              'pop': [{'species': 'Herbivore',
                       'age': 5,
                       'weight': 50}
                      for _ in range(400)]}]

sim = BioSim(geogr, ini_herbs, seed=123, img_dir='results', img_base='migration_checkboard',
             vis_years=1)
sim.set_animal_parameters('Herbivore', {'mu': 1, 'omega': 0, 'gamma': 0, 'a_half': 1000})
sim.set_animal_parameters('Carnivore', {'mu': 1, 'omega': 0, 'gamma': 0,
                                        'F': 0, 'a_half': 1000})
sim.simulate(8)
