# -*- coding: utf-8 -*-

import textwrap
import random
from biosim.landscape import Lowland, Highland, Desert, Water


class Island:
    """Superclass simulating the entire island with its attributes defined in previous files.

    Parameters
    -------------
    geo : str
      Multi string map encapsulated by 3 '"' in both ends. Can include a '\' in the start or end.
    ini_pop : list
      List of dictionaries, each dict is for one loc. Each loc is on format (tuple), example:
      (x,y), where x=row, y=column. Dicts on format: {"loc": (x,y), "pop": []}. Within the
      "pop"-part, are dictionaries for animals, on the format: {"species": str, "age": int,
      "weight": float}

    Attributes
    -------------
    islandmap : dict
      Keys = tuple (x,y). Values = instance of a landscape type, with input = list of animal-dicts.
      The animal dicts are on format: {"species": str, "age": int, "weight": float}
    geography : dict
      Keys = tuple (x,y). Values = str indicating landscape type (e.g. "H" for highland)
    population : dict
      Keys = tuple (x,y). Values = list of dictionaries. Each dict is on format: {"species": str,
      "age": int, "weight": float}
    geogr : str
      Same as the input geo, a multi-line string encapsulated by '"' and maybe a backslash
    df : pandas dataframe
      A df where the index and column names both range from 1. Within each cell is a letter/string,
      giving the landscape type (e.g. "W" for water)
    keys_total : list
      List of tuples, yielding all locations that are represented in the multi-string map

    """

    def __init__(self, geo, ini_pop):
        self.islandmap = {}
        self.geography = {}
        self.population = {}
        self.geogr = geo
        self.ini_pop = ini_pop
        self.df = self.preprocess_map()

        # Test if all outer-cells in the square map is "W"
        col1 = self.df.iloc[:, 0]
        cole = self.df.iloc[:, -1]
        row1 = self.df.iloc[0, :]
        rowe = self.df.iloc[-1, :]

        if not (all(col1 == "W") and all(cole == "W") and all(row1 == "W") and all(rowe == "W")):
            raise ValueError("All outer edge cells should be water (W) cells")

        # Test of all values in the map are either "W", "D", "L" or "H"
        # Make a list of True/False for each row (row_index), check if ALL values in the list is
        #  True. Result = the list "all_in", with one True/False for each row in the map
        all_in = [all([(val in ["W", "H", "L", "D"]) for val in self.df.iloc[row_i, :]])
                  for row_i in range(self.df.shape[0])]

        if not all(all_in):
            raise ValueError("All cells in the map should be either W, D, L or H")

        self.preprocess_map_dict()  # Updating self.geography
        self.keys_total = list(self.geography.keys())

    def preprocess_map(self, empty=False):
        """Converting the multi-string geo-map into a pandas dataframe indexed from 1 onwards

        Parameters
        ----------
        empty : bool
          Asserts whether we want returned a dataframe where the values as either biomes
          (empty=False), or as an empty (all 0's, empty=True) dataframe. The latter relates to
          visualization


        Returns
        ----------
        df : pandas dataframe
          A df indexed from 1--> in both row and columns. Each cell has info on the landscape type

        """
        import pandas as pd
        geog = textwrap.dedent(self.geogr)
        geogr_strlist = [string.replace(" ", "").replace("\\", "") for string in geog.splitlines()]
        df = pd.DataFrame(geogr_strlist)
        df = df[0].str.split('', 0, expand=True)  # first (and only) column split at each character
        df = df.iloc[:, 1:-1]  # remove first and last col, no geography stored here
        df.index = range(1, df.shape[0] + 1)  # redefine index from 0--> to 1-->

        if ("" in df.values) is False:
            if not empty:
                return df
            else:
                empty_df = df.copy()
                for col in empty_df.columns:
                    empty_df[col].values[:] = 0
                return empty_df

        else:
            raise ValueError('The map is of inconsistent size.')

    def preprocess_map_dict(self):
        """
        Connects the coordinate with its respective landscape type, in the shape of a dictionary.
        Example of return: {(1, 1): 'W', (1, 2): 'L', ...}

        Parameters
        ------------
        df : pandas dataframe
          The dataaframe made by "preprocess_map", e.g a dataframe of the map indexed from 1-->

        Returns
        ------------
        Updated geography : dict
          Updated geography, yielding keys as coordinates and values as landscape strings

        """

        for x_idx in range(1, self.df.shape[0] + 1):
            for y_idx in range(1, self.df.shape[1] + 1):
                coord = (x_idx, y_idx)
                biome = self.df.loc[coord]
                self.geography[coord] = biome

    def convert_pop_to_dict(self):
        """Converts the input list of populations to a dictionary. The keys are the coordinates, and
        the values are the list of animals. This is necessary when simulating using (ini_carns +
        ini_herbs).

        Parameters
        -----------
        ini_pop : list
          List of dictionaries, each on format {"loc": tuple, "pop": list}

        Returns
        -----------
        updated population : dict
          Converted the initial population, from {"loc": tuple, "pop": list} to {tuple, list}
        """

        for initial_population in self.ini_pop:
            if initial_population['loc'] in self.population.keys():
                self.population[initial_population['loc']].extend(initial_population['pop'])
            else:
                self.population[initial_population['loc']] = initial_population['pop']

    def assign_location(self):
        """
        Chooses random x- and y-locations on the map, leading to a non-watercell, to give to
        animals with loc = None.

        Returns
        -----------
        location : tuple
          Tuple (x,y), where x = row, y = col, randomly chosen BUT it's NOT a "W" cell

        """
        the_map = self.preprocess_map()
        is_water = True

        while is_water:
            x_coord = random.choice(range(1, the_map.shape[0] + 1))
            y_coord = random.choice(range(1, the_map.shape[1] + 1))

            location = (x_coord, y_coord)
            biom = the_map.loc[location]

            try:
                if biom.upper() != "W":
                    if biom.upper() in ["D", "H", "L"]:
                        is_water = False
                        return location
                    else:
                        raise ValueError("Viable values in the multi-string map = [D, H, L, W]")
            except AttributeError:
                raise AttributeError("Values in 'geography' must be strings")

    def add_population(self, population):
        """
        Add new animals to the existing population

        Parameters
        ---------------
        population : list
          List of dictionaries, each on format: {"loc": (x, y), "pop": [{..}, {..}, ..]}. The "pop"
          part is a list with dicts, where each dict is on format: {"species": str, "age": int,
          "weight": float}

        Returns
        ----------------
        Updated islandmap : dict
          Dict on format keys=loc, values=landscape instance. Might look like:
          {(x, y): Highland( [{"species": "Herbivore", "age": 2, "weight": 5.5}] )}

        """

        for loc, instances in self.islandmap.items():
            # for (x, y) and landscape-instance ...
            for list_of_elements in population:
                # for loc-pop dictionaries in input "population" ...
                loc_of_added = list_of_elements["loc"]

                if loc_of_added is None:
                    loc_of_added = self.assign_location()
                if loc_of_added not in self.islandmap.keys():
                    pass
                if loc_of_added == loc and self.islandmap[loc].is_land:
                    instances.add_animals(list_of_elements['pop'])

    def create_islandmap(self):
        """
        Final map in the form of a dictionary, where keys are coordinates and values are
        instances of landscapes, which includes instances of the animals.

        Returns
        ------------
        Updated islandmap : dict
          Keys = tuple (x,y), Values = landscape type instance. Each location in islandmap is
          assigned an instance of a landscape type
        """

        self.preprocess_map_dict()  # create self.geography
        self.convert_pop_to_dict()  # convert ini_pop to self.population dictionary

        for loc, land in self.geography.items():
            # for (x,y), biom type ("D", "W", ...) in this dict ...
            if land == 'H':
                for animal_coordinate in self.population.keys():
                    # for (x,y) in population (that's made from ini_pop) ...
                    if animal_coordinate == loc:
                        # put the list of "species"-"age"-"weight" dicts into a landscape instance
                        self.islandmap[loc] = Highland(self.population[loc])
                    else:
                        self.islandmap[loc] = Highland([])

            elif land == "L":
                for animal_coordinate in self.population.keys():
                    if animal_coordinate == loc:
                        self.islandmap[loc] = Lowland(self.population[loc])
                    else:
                        self.islandmap[loc] = Lowland([])

            elif land == "D":
                for animal_coordinate in self.population.keys():
                    if animal_coordinate == loc:
                        self.islandmap[loc] = Desert(self.population[loc])
                    else:
                        self.islandmap[loc] = Desert([])

            elif land == "W":
                for animal_coordinate in self.population.keys():
                    self.islandmap[loc] = Water([])

        # find the neighbors and append them to their respective landscape instance.
        for loc, land in self.islandmap.items():
            left_loc = (loc[0], loc[1]-1)
            up_loc = (loc[0] + 1, loc[1])
            right_loc = (loc[0], loc[1]+1)
            down_loc = (loc[0] - 1, loc[1])
            for dir_locs in [left_loc, up_loc, right_loc, down_loc]:
                if dir_locs in self.islandmap.keys():
                    if self.islandmap[dir_locs].is_land:
                        land.neighbors.append(self.islandmap[dir_locs])

    def run_one_season(self):
        """
        Simulating one season. This method will run all the seasons in correct order.
        Returns various updates regarding weight, fitness, and number of animals in each cell
        """

        for loc, landscape_instance in self.islandmap.items():
            landscape_instance.feed_herbivores()
            landscape_instance.feed_carnivores()
            landscape_instance.give_birth_in_cell()
        # re-iterate for migration to correctly update the instances.
        for loc, landscape_instance in self.islandmap.items():
            landscape_instance.migration_herb()
            landscape_instance.migration_carn()
        for loc, landscape_instance in self.islandmap.items():
            landscape_instance.age_all_animals()
            landscape_instance.remove_weight()
            landscape_instance.kill_animals()
