# -*- coding: utf-8 -*-

import random
import math


class Animals:
    """A class representing the animals. Animals is the parent class, and herbivores and carnivores
     inherit its constructor and methods.

    Parameters
    -------------
    values : dict
      Contain properties of an animal ("species", "age", "weight")

    Attributes
    -------------
    fitness : float
      Numeric value, dependent on age and weight, asserting how viable the animal is
    alive : boolean
      If the animal is alive or not

    """
    parameters = None

    @classmethod
    def set_animal_params(cls, params):
        """Sets the animal parameters to the input parameters."""

        for param_name, param_value in params.items():
            if param_name not in cls.parameters:
                raise KeyError('Invalid parameter {}.'.format(param_name))
            if param_name == 'DeltaPhiMax':
                if params[param_name] <= 0:
                    raise ValueError('Parameter {} has to be strictly positive'.format(param_name))
            if param_name != 'DeltaPhiMax':
                if params[param_name] < 0:
                    raise ValueError('Parameter {} has to be positive or zero'.format(param_name))
            if param_name == 'eta':
                if params[param_name] > 1:
                    raise ValueError('Parameter {} has to be less than one'.format(param_name))

            else:
                cls.parameters[param_name] = params[param_name]

    @classmethod
    def get_default_params(cls):
        """Returns the default parameters."""

        return cls.parameters.copy()

    def __init__(self, values):
        if values['age'] < 0:
            raise ValueError('Age has to be a non-negative number')
        elif values['weight'] < 0:
            raise ValueError('Weight has to be a non-negative integer')

        self.weight = values['weight']
        self.age = values['age']
        self.fitness = None
        self.update_fitness()
        self.alive = True
        self.animal_has_moved = False

    def update_fitness(self):
        """Set an animal's fitness with respect to weight, age and species."""

        q_plus = 1 / (1 + math.exp(
            self.parameters['phi_age'] * (self.age - self.parameters['a_half'])))
        q_minus = 1 / (1 + math.exp(
            -self.parameters['phi_weight'] * (self.weight - self.parameters['w_half'])))
        if self.weight == 0:
            self.fitness = 0
        else:
            self.fitness = q_plus * q_minus

    def will_animal_give_birth(self, num_animals):
        """
        Determine if an animal will give birth.

        Parameters
        ------------
        num_animals : int
          Defined for >= 0, number of animals

        Returns
        ------------
        if_birth : boolean
          Asserts whether conditions & randomness will allow the animal to give birth this year

        """

        if self.weight < self.parameters['zeta']*(self.parameters['w_birth']
                                                  + self.parameters['sigma_birth']):
            probability = 0
        else:
            probability = min([1, self.parameters['gamma']*self.fitness*(num_animals-1)])

        return random.random() <= probability

    def give_birth(self, num_animals):
        """
        Simulate birth of one animal.

        Parameters
        ------------
        num_animals : int
          Defined for >= 0 number of animals

        Returns
        -----------
        baby_weight : float
          Weight of the newborn animal, if birth takes place

        """

        baby_weight = abs(random.gauss(self.parameters['w_birth'],
                                       self.parameters['sigma_birth']))
        if self.will_animal_give_birth(num_animals) and self.weight > self.parameters['xi'] *\
                baby_weight:
            self.weight -= self.parameters['xi']*baby_weight
            self.update_fitness()
            return baby_weight

    def will_migrate(self):
        """A method to determine if an animal will migrate.

        Returns
        ----------
        True / False, e.g whether the animal will move.
        """

        self.update_fitness()
        prob_moves = self.parameters["mu"] * self.fitness

        if not self.animal_has_moved:
            r = random.random()
            return r < prob_moves
        else:
            return False

    def add_age(self):
        """Add a year to the age of the animal instance. Also updates fitness."""

        self.age += 1
        self.update_fitness()

    def weight_loss(self):
        """Update weight from annual weight loss. Reset that the animal has moved, such that
        in the next year it will move again. Also updates fitness."""

        self.weight -= self.parameters['eta']*self.weight
        self.update_fitness()
        self.animal_has_moved = False

    def death(self):
        """Determine if an animal dies. Depends on fitness, or if weight is 0."""

        prob_death = self.parameters["omega"]*(1-self.fitness)

        if self.weight == 0:
            self.alive = False

        r = random.random()
        if r <= prob_death:
            self.alive = False


class Herbivore(Animals):
    """DESCRIPTION: Child class of Animals. Inherits the properties from Animals.
    """

    parameters = {'w_birth': 8.0,
                  'sigma_birth': 1.5,
                  'beta': 0.9,
                  'eta': 0.05,
                  'a_half': 40.0,
                  'phi_age': 0.6,
                  'w_half': 10.0,
                  'phi_weight': 0.1,
                  'mu': 0.25,
                  'gamma': 0.2,
                  'zeta': 3.5,
                  'xi': 1.2,
                  'omega': 0.4,
                  'F': 10.0}

    def __init__(self, values):
        super().__init__(values)

    def update_weight_from_fodder(self, consumed_fodder):
        """
        Update the weight of an animal after it consumes an amount of fodder. Also
        updates fitness.

        Parameters
        --------------
        consumed_fodder: float
           amount of fodder consumed

        Returns
        ---------------
        updated animal weight.

        """

        self.weight += self.parameters['beta'] * consumed_fodder
        self.update_fitness()


class Carnivore(Animals):
    """DESCRIPTION: Child class of Animals. Inherits the properties from Animals.
    """

    parameters = {'w_birth': 6.0,
                  'sigma_birth': 1.0,
                  'beta': 0.75,
                  'eta': 0.125,
                  'a_half': 40.0,
                  'phi_age': 0.3,
                  'w_half': 4.0,
                  'phi_weight': 0.4,
                  'mu': 0.4,
                  'gamma': 0.8,
                  'zeta': 3.5,
                  'xi': 1.1,
                  'omega': 0.8,
                  'F': 50.0,
                  'DeltaPhiMax': 10.0
                  }

    def __init__(self, values):
        super().__init__(values)

    def update_weight_from_herbi(self, herbivore_weight):
        """
        Given the weight of the herbivore that the carnivore eats, update the carnivore's weight
        and fitness.

        Parameters
        ----------
        herbivore_weight : float
          The weight of a herbivore (eaten by the carnivore)

        Returns
        -------
        Updated weight : various
          Updated weight, and fitness, of the carnivore

        """

        self.weight += self.parameters['beta'] * herbivore_weight
        self.update_fitness()

    def will_carn_eat_herb(self, herbivore):
        """
        Determine if a carnivore will eat the herbivore.

        Parameters
        ------------
        herbivore : instance
          An instance of the class Herbivore

        Returns
        -----------
        will_eat : boolean
          Whether the carnivore will successfully kill and eat the herbivore

        """
        if self.parameters['F'] == 0:
            probability = 0
        elif self.fitness < herbivore.fitness:
            probability = 0
        elif 0 < self.fitness - herbivore.fitness < self.parameters['DeltaPhiMax']:
            probability = (self.fitness - herbivore.fitness) / self.parameters['DeltaPhiMax']
        else:
            probability = 1

        will_eat = False
        if random.random() <= probability:
            will_eat = True

        return will_eat

    def attempt_to_eat_herb(self, list_of_herbs):
        """
        A method to simulate carnivores eating herbivores.
        Parameters
        --------------
        list_of_herbs : list
          List of instances of the Herbivore class

        Returns
        --------------
        eaten_herbis : list
          List of instances of the Herbivore class that were eaten.

        """

        eaten_herbis = []
        f_val = Carnivore.parameters['F']

        for herbi in list_of_herbs:
            tot_weight_consumed = 0
            if self.will_carn_eat_herb(herbi):
                w_h = herbi.weight
                if tot_weight_consumed <= f_val:
                    self.update_weight_from_herbi(herbi.weight)
                    eaten_herbis.append(herbi)
                    tot_weight_consumed += herbi.weight
                # if the herbivore weighs more than the carnivore desires, it will only eat
                # what it wants, and the remaining goes to waste.
                elif tot_weight_consumed <= f_val and (w_h+tot_weight_consumed) >= f_val:
                    self.update_weight_from_herbi(f_val - tot_weight_consumed)
                    eaten_herbis.append(herbi)
                    tot_weight_consumed += (f_val-tot_weight_consumed)
        return eaten_herbis
