# -*- coding: utf-8 -*-

from .animals import Herbivore, Carnivore
import random


class Landscape:
    """Landscape class representing a single cell, containing instances of animals.

    Parameters
    -------------
    animal_population : list
      A list of dictionaries (dicts with values for "species", "age", "weight") of different
      animals


    Attributes
    -------------
    herbivores : list
      List of animal instances of the Herbivore class
    carnivores : list
      List of animal instances of the Carnivore class
    fodder_available : float
      Relative fodder left in the landscape cell, changes when herbivores eats
    max_fodder : float
      Equals f_max, different for each type of landscape

    """
    is_land = True
    parameters = None

    @classmethod
    def set_landscape_params(cls, params):
        for param_name, param_value in params.items():
            if param_name not in cls.parameters.keys():
                raise KeyError('Param {} is not in any of the landscape instances'.format
                               (param_name))
            if param_value < 0:
                raise ValueError('The fodder available on each cell has to be either 0 or positive')
            else:
                cls.parameters[param_name] = params[param_name]

    def __init__(self, animal_population):
        """The constructor for Landscape converts a list of animals into respective lists
        depending on their species. It is a element of populations in 3.3.3 from proj.description

         Example of animal_population:
             [{"species": "Herbivore", "age": 10, "weight": 12.5},
              { ...another animal... }
             ]
         """

        self.herbivores = []
        self.carnivores = []
        self.available_fodder = 0

        self.add_animals(animal_population)
        self.sort_animals_by_fitness()
        self.neighbors = []

    def add_animals(self, animals):
        """Add each animal to their respective list in the current landscape instance."""

        for animal in animals:
            if animal['species'] == 'Herbivore':
                self.herbivores.append(Herbivore(animal))
            elif animal['species'] == 'Carnivore':
                self.carnivores.append(Carnivore(animal))
            else:
                raise ValueError("species should be Herbivore or Carnivore")

    def annual_growth(self):
        """Sets the max_fodder attribute to f_max again"""

        self.available_fodder = self.parameters['f_max']

    def sort_animals_by_fitness(self):
        """Sorts animals by fitness. Carnivores by default will be sorted from high
        to low fitness."""

        self.herbivores = sorted(self.herbivores, key=lambda x: x.fitness, reverse=False)
        self.carnivores = sorted(self.carnivores, key=lambda x: x.fitness, reverse=True)

    def feed_herbivores(self):
        """Annual growth of fodder every season. Herbivores eat and fodder_available will deplete
         when they eat. The herbivores will also have their weight updated accordingly."""

        self.annual_growth()
        desired_fodder = Herbivore.parameters['F']
        random.shuffle(self.herbivores)

        for herbi in self.herbivores:
            if self.available_fodder >= desired_fodder:
                herbi.update_weight_from_fodder(desired_fodder)
                self.available_fodder -= desired_fodder
            elif desired_fodder > self.available_fodder:
                herbi.update_weight_from_fodder(self.available_fodder)
                self.available_fodder = 0

    def feed_carnivores(self):
        """Attempt to kill all herbivores in the list of herbivores. This list should be sorted
        by increasing fitness. The herbivores-list is updated, removing the ones eaten."""

        self.sort_animals_by_fitness()

        for carni in self.carnivores:  # starting with the carni with highest fitness.
            eaten_herbis = carni.attempt_to_eat_herb(self.herbivores)
            self.herbivores = [herb for herb in self.herbivores if herb not in eaten_herbis]

    def age_all_animals(self):
        """Age all animals. Fitness is updated in the Animals class."""

        for herbi in self.herbivores:
            herbi.add_age()

        for carni in self.carnivores:
            carni.add_age()

    def give_birth_in_cell(self):
        """Calculates if birth happens for all the animals in our
        instance, and extends the current animal list with the new babies."""

        newborn_herbi = []
        for herbi in self.herbivores:
            baby_weight = herbi.give_birth(len(self.herbivores))
            if baby_weight is not None:
                newborn_herbi.append(Herbivore({'species': 'Herbivore',
                                                'age': 0,
                                                'weight': baby_weight}))
        self.herbivores.extend(newborn_herbi)

        newborn_carni = []
        for carni in self.carnivores:
            baby_weight = carni.give_birth(len(self.carnivores))
            if baby_weight is not None:
                newborn_carni.append(Carnivore({'species': 'Carnivore',
                                                'age': 0,
                                                'weight': baby_weight}))
        self.carnivores.extend(newborn_carni)

    def kill_animals(self):
        """For each animal, tests if they die. Updates the animal list, removing
        those where death took place."""

        for herbi in self.herbivores:
            herbi.death()
        self.herbivores = [herb for herb in self.herbivores if herb.alive]

        for carni in self.carnivores:
            carni.death()
        self.carnivores = [carn for carn in self.carnivores if carn.alive]

    def remove_weight(self):
        """Annually removes weight from each animal"""

        for animal in self.herbivores + self.carnivores:
            animal.weight_loss()

    def migration_herb(self):
        """Attempt to migrate all herbivores, where they have the possibility to
         move to a random cell neighbour."""

        removed = []
        for herb in self.herbivores:
            will_migrate = herb.will_migrate()
            if will_migrate:
                # self.neighbors ~ list of landscape instances.
                # The elements are: [0] = left, [1] = up, [2] = right, [3] = down
                if len(self.neighbors) >= 1:
                    new_land = random.choice(self.neighbors)  # choose random neighbor cell
                    herb.animal_has_moved = True
                    new_land.herbivores.append(herb)
                    removed.append(herb)
        # remove animals from the current instance if they moved.
        for herb in removed:
            self.herbivores.remove(herb)

    def migration_carn(self):
        """Attempt to migrate all carnivores, where they have the possibility to
         move to a random cell neighbour."""

        removed = []
        for carn in self.carnivores:
            will_migrate = carn.will_migrate()
            if will_migrate:
                if len(self.neighbors) >= 1:
                    new_land = random.choice(self.neighbors)
                    carn.animal_has_moved = True
                    new_land.carnivores.append(carn)
                    removed.append(carn)

        for carn in removed:
            self.carnivores.remove(carn)


class Lowland(Landscape):
    """
    Parameters
    ------------
    f_max : int
    Max fodder available (for herbivores) in this type of cell

    """

    parameters = {'f_max': 800}

    def __init__(self, animal_population):
        super().__init__(animal_population)


class Highland(Landscape):
    """
    Parameters
    ------------
    f_max : int
      Max fodder available (for herbivores) in this type of cell. Less fodder than in lowland.

    """
    parameters = {'f_max': 300}

    def __init__(self, animal_population):
        super().__init__(animal_population)


class Desert(Landscape):
    """
    Parameters
    ------------
    f_max : int
      Max fodder available (for herbivores) in this type of cell. By default 0.

    """
    parameters = {'f_max': 0}

    def __init__(self, animal_population):
        super().__init__(animal_population)


class Water(Landscape):
    """
    Parameters
    ------------
    f_max : int
      Max fodder available (for herbivores) in this type of cell. No fodder in water cell.

    """
    is_land = False
    parameters = {'f_max': 0}

    def __init__(self, animal_population):
        super().__init__(animal_population)
