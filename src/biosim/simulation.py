# -*- coding: utf-8 -*-
#import sys
#sys.path.append("src")
from biosim.island import Island
from biosim.landscape import Lowland, Highland, Desert, Water
from biosim.animals import Herbivore, Carnivore
import matplotlib.pyplot as plt
import numpy as np
import os
import subprocess
import random

# The material in this file is licensed under the BSD 3-clause license
# https://opensource.org/licenses/BSD-3-Clauseim
# (C) Copyright 2021 Hans Ekkehard Plesser / NMBU
# (C) Copyright 2021 Erlend Kristiansen Risvik and Mina Therese Gjefle / NMBU

_FFMPEG_BINARY = 'ffmpeg'
_MAGICK_BINARY = 'magick'

# update this to the directory and file-name beginning
# for the graphics files
_DEFAULT_GRAPHICS_DIR = os.path.join('..', 'data')
_DEFAULT_GRAPHICS_NAME = 'dv'
_DEFAULT_IMG_FORMAT = 'png'
_DEFAULT_MOVIE_FORMAT = 'mp4'   # alternatives: mp4, gif


class BioSim:
    """
    Masterclass used to simulate life on an island

    Parameters
    ------------
    island_map : str
      Multi-line string encapsulated by 3 '"' and possibly a backslash
    ini_pop : list
      List of dictionaries specifying initial population. Each dict on format:
      {"loc": tuple, "pop": list}, where the list contain dictionaries, each on format:
      {"species": str, "age": int, "weight": float}
    seed : int
      Random seed to give reproducibility
    vis_years : int
      years between visualization updates (if 0, disable graphics)
    ymax_animals : int
      Number specifying y-axis limit for graph showing animal numbers. If None, the y-axis limit
      should be adjusted automatically
    cmax_animals : dict
      Dict specifying color-code limits for animal densities. If None, then sensible, fixed default
      values should be used. This is a dict mapping species names to numbers, e.g.,
      {’Herbivore’: 50, ’Carnivore’: 20}
    hist_specs : dict
      Specifications for histograms. This is a dictionary with one entry per property for which a
      histogram shall be shown. For each property, a dictionary providing the maximum value and the
      bin width must be given, e.g., {’weight’: {’max’: 80, ’delta’: 2}, ’fitness’:
      {’max’: 1.0, ’delta’: 0.05}}. Permitted properties are ’weight’, ’age’, ’fitness’.
    img_dir : str
      String with path to directory for figures. If None, no figures are written to file.
      If not None, then both img_dir and img_base should be a string.
      Filenames are formed as f’{os.path.join(img_dir, img_base}_{img_number:05d}.{img_fmt}’
      where img_number are consecutive image numbers starting from 0.
    img_base : str
      String with beginning of file name for figures. If not None, then both img_base and img_dir
      should be a string.
    img_fmt : str
      String with file type for figures, e.g. ’png’
    img_years : int
      years between visualizations saved to files (default: vis_years)
    log_file : str
      If given, write animal counts to this file

    Attributes
    ------------
    island : instance
      Instance of the class Island, with island_map and ini_pop put into
    geogr : str
      Same as island_map
    geogr_array : array
      Array of island_map, where each element (each list) is a row from the map, with string-values
      specifying the biome type
    years_simulated : int
      A counter-variable, counting number of years
    final_year : int
      Last year of simulation
    _num_img : int
      A counter-variable, counting number of images made
    _img_base : os path
      Os path object, made from img_dir and img_base
    _img_ctr : int
    _fig : plot
      A plot object
    _map_ax : axis
      Axis on _fig, for plotting of the static map
    _animal_line_ax : axis
      Axis on _fig, for plotting the animal graphs counting number of animals
    _herbi_line_plot : plot
    _carni_line_plot : plot
    _heatmap_herb_ax : axis
      Axis on _fig, for plotting of the dynamic heatmap-style plot of the map, showing population
      density for herbivores across the map
    _heatmap_carn_ax : axis
      Axis on _fig, for plotting dynamic heatmap-style plot of the map, showing population density
      for carnivores across the map
    _hist_fitness_ax : axis
      Axis on _fig, for plotting histogram lines for fitness distribution
    _hist_age_ax : axis
      Axis on _fig, for plotting histogram lines for age distribution
    _hist_weight_ax : axis
      Axis on _fig, for plotting histogram lines for weight distribution

    """

    def __init__(self, island_map, ini_pop, seed=123, vis_years=1, ymax_animals=None,
                 cmax_animals=None, hist_specs=None,
                 img_dir=None, img_base=None, img_fmt='png', img_years=None, log_file=None):

        # Island part
        random.seed(seed)
        self.island = Island(island_map, ini_pop)
        self.island.create_islandmap()
        self.geogr = island_map

        # Simulation part
        self.years_simulated = 0
        self.final_year = None

        # Graphics specifications
        self._ymax_animals = ymax_animals
        self._cmax_animals = cmax_animals
        self._hist_specs = hist_specs
        self._num_img = 0
        if img_dir is not None:
            self._img_base = os.path.join(img_dir, img_base)
        else:
            self._img_base = None
        self._img_fmt = img_fmt
        self._vis_years = vis_years
        self._img_years = img_years

        # Graphic plots
        self._fig = None  # the master figure
        self._map_ax = None  # island map
        self._animal_line_ax = None  # line ax for both species
        self._herbi_line_plot = None  # the herbivore line
        self._carni_line_plot = None  # the carnivore line
        self._heatmap_herb_ax = None  # herbivore heatmap
        self._heatmap_plot_herb = None  # heatmap herbivore plot
        self._heatmap_carn_ax = None  # heatmap carnivore
        self._heatmap_plot_carn = None  # heatmap carnivore plot
        self._hist_fitness_ax = None   # histogram fitness
        self._hist_age_ax = None  # histogram age
        self._hist_weight_ax = None  # histogram weight

    @staticmethod
    def set_animal_parameters(species, params):
        """
        Set parameters for animal species.

        Parameters
        ------------
        species : str
          Name of animal species, e.g. "Herbivore" or "Carnivore"
        params : dict
          Dict with valid parameter specification for species

        """

        animal_instances = {"Herbivore": Herbivore,
                            "Carnivore": Carnivore}
        animal_instances[species].set_animal_params(params)

    @staticmethod
    def set_landscape_parameters(landscape, params):
        """
        Set parameters for landscape type.

        Parameters
        ------------
        landscape : str
          Code letter for landscape type, e.g. "H", "L", "D", "W"
        params : dict
          Dict with valid parameter specification for landscape

        """
        landscape_instances = {'L': Lowland,
                               'H': Highland,
                               'D': Desert,
                               'W': Water}
        landscape_instances[landscape].set_landscape_params(params)

    def simulate(self, num_years):
        """
        Run simulation while visualizing the result.

        Parameters
        ------------
        num_years : int
          number of years to simulate

        Returns
        --------
        updated years_simulated : int
          Adds 1
        setups : various
          Runs: setup_graphics, update_graphics, and run_one_season

        """
        if self._img_years is None:
            self._img_years = self._vis_years

        self.final_year = self.year + num_years
        if self._vis_years > 0:
            self.setup_graphics()

        while self.year < self.final_year:
            if self._vis_years != 0:
                if self.years_simulated % self._vis_years == 0:
                    self.update_graphics()
                if self.years_simulated % self._img_years == 0:
                    self.save_graphics()

            self.island.run_one_season()
            self.years_simulated += 1

    def add_population(self, population):
        """
        Add a population to the island

        Parameters
        --------------
        population : list
           List of dictionaries specifying population. Each dict is on format: {(x,y): list}, where
           each list contain dictionaries that are on format: {"species": int, "age": int,
           "weight": float}

        """
        return self.island.add_population(population)

    @property
    def year(self):
        """Last year simulated.

        Returns
        ----------
        years_simulated : int

        """
        return self.years_simulated

    @property
    def num_animals(self):
        """Total number of animals on island.

        Returns
        ----------
        num_animals : int
          Number of animals total, both carnivores and herbivores

        """
        num_animals = 0

        for animal_list in self.island.islandmap.values():
            num_animals += len(animal_list.herbivores)
            num_animals += len(animal_list.carnivores)

        return num_animals

    @property
    def num_animals_per_species(self):
        """Number of animals per species in the island, as a dictionary.

        Returns
        ----------
        num_animals_per_species : dict
          Dict with number of animals per species total. Dict is on format: {"num_herbs": int,
          "num_carns": int}

        """
        num_animals_per_species = {'Herbivore': 0,
                                   'Carnivore': 0
                                   }

        for animal_lists in self.island.islandmap.values():
            num_animals_per_species['Herbivore'] += len(animal_lists.herbivores)
            num_animals_per_species['Carnivore'] += len(animal_lists.carnivores)
        return num_animals_per_species

    @property
    def herb_spread(self):
        """
        Counting number of herbivores in each cell of the map.

        Returns
        -------
        empty_df_herb : pandas dataframe
          Dataframe where columns and rows are indexed from 1-->. Values in cells correspond to
          the number of herbivores in that cell at that moment.

        """
        empty_df_herb = self.island.preprocess_map(empty=True)

        for loc_keys in self.island.islandmap.keys():
            # Iterates through all locs in the map
            empty_df_herb.loc[loc_keys] = len(self.island.islandmap[loc_keys].herbivores)
        return empty_df_herb

    @property
    def carn_spread(self):
        """
        Counting number of carnivores in each cell of the map.

        Returns
        -------
        empty_df_carn : pandas dataframe
          Dataframe where columns and rows are indexed from 1-->. Values in cells correspond to
          the number of carnivores in that cell at that moment.

        """
        empty_df_carn = self.island.preprocess_map(empty=True)

        for loc_keys in self.island.islandmap.keys():
            # Iterates through all locs in the map
            empty_df_carn.loc[loc_keys] = len(self.island.islandmap[loc_keys].carnivores)
        return empty_df_carn

    @property
    def animal_features_per_species(self):
        """
        Store the feature distribution for both species, in a nested dictionary for accessibility.
        Returns
        -------
        features : dict
          Dict where key = species type, value = dict, e.g. the dict is on format:
          {"Herbivore" : dict, "Carnivore" : dict}, where each of the inner dicts are on the
          format: {"Age": list, "Weight": list, "Fitness": list}

        """
        features = {'Herbivore': {'Age': [],
                                  'Weight': [],
                                  'Fitness': []},
                    'Carnivore': {'Age': [],
                                  'Weight': [],
                                  'Fitness': []}}
        for landscape in self.island.islandmap.values():
            for herbis in landscape.herbivores:
                features['Herbivore']['Age'].append(herbis.age)
                features['Herbivore']['Weight'].append(herbis.weight)
                features['Herbivore']['Fitness'].append(herbis.fitness)

            for carnis in landscape.carnivores:
                features['Carnivore']['Age'].append(carnis.age)
                features['Carnivore']['Weight'].append(carnis.weight)
                features['Carnivore']['Fitness'].append(carnis.fitness)
        return features

    def make_movie(self, movie_fmt=_DEFAULT_MOVIE_FORMAT):
        """
        Creates MPEG4 movie from visualization images saved.
        """

        if self._img_base is None:
            raise RuntimeError("No filename defined.")

        if movie_fmt == 'mp4':
            try:
                # Parameters chosen according to http://trac.ffmpeg.org/wiki/Encode/H.264,
                # section "Compatibility"
                subprocess.check_call([_FFMPEG_BINARY,
                                       '-i', '{}_%05d.png'.format(self._img_base),
                                       '-y',
                                       '-profile:v', 'baseline',
                                       '-level', '3.0',
                                       '-pix_fmt', 'yuv420p',
                                       '{}.{}'.format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: ffmpeg failed with: {}'.format(err))
        elif movie_fmt == 'gif':
            try:
                subprocess.check_call([_MAGICK_BINARY,
                                       '-delay', '1',
                                       '-loop', '0',
                                       '{}_*.png'.format(self._img_base),
                                       '{}.{}'.format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: convert failed with: {}'.format(err))
        else:
            raise ValueError('Unknown movie format: ' + movie_fmt)

    def save_graphics(self):
        """Saves graphics to file if file name is given.

        Returns
        ----------
        Updated _num_img : int
          Adds 1

        """

        if self._img_base is None:
            return

        plt.savefig('{base}_{num:05d}.{type}'.format(base=self._img_base,
                                                     num=self._num_img,
                                                     type=self._img_fmt))
        self._num_img += 1

    def setup_graphics(self):
        """
        A method to construct the graphic window with our subplots.

        Returns
        ---------
        Updated plot : plot
          Updates _fig, and all axis-objects mentioned in constructor.
        """

        # create new figure window
        if self._fig is None:
            self._fig = plt.figure(figsize=(13, 10))

        if self._map_ax is None:
            self._map_ax = self._fig.add_subplot(3, 3, 1)
            self.create_map()

        if self._hist_age_ax is None:
            self._hist_age_ax = self._fig.add_subplot(6, 3, 16)

        if self._hist_fitness_ax is None:
            self._hist_fitness_ax = self._fig.add_subplot(6, 3, 17)

        if self._hist_weight_ax is None:
            self._hist_weight_ax = self._fig.add_subplot(6, 3, 18)
        self.create_hists()

        if self._animal_line_ax is None:
            self._animal_line_ax = self._fig.add_subplot(3, 3, 3)

        self._animal_line_ax.set_xlim(0, self.final_year + 1)
        self.create_animal_lines()

        if self._heatmap_herb_ax is None:
            self._heatmap_herb_ax = self._fig.add_subplot(3, 3, 4)
            self._heatmap_plot_herb = None

        if self._heatmap_carn_ax is None:
            self._heatmap_carn_ax = self._fig.add_subplot(3, 3, 6)
            self._heatmap_plot_carn = None

        self._fig.tight_layout(pad=3.0)

    def update_heatmaps(self):
        """Updates the heatmap figure by plotting the current distribution for both animal
        species on the island.
        """

        if self._cmax_animals:
            c_max_herb = self._cmax_animals['Herbivore']
            c_max_carn = self._cmax_animals['Carnivore']
        else:
            c_max_herb = 100
            c_max_carn = 100
        herb_data = self.herb_spread.values.astype(int)
        carn_data = self.carn_spread.values.astype(int)

        if self._heatmap_plot_herb is not None:
            self._heatmap_plot_herb.set_data(herb_data)
        else:
            self._heatmap_plot_herb = self._heatmap_herb_ax.imshow(herb_data, cmap="Greens",
                                                                   interpolation='nearest',
                                                                   vmin=-0, vmax=c_max_herb)
            self._fig.colorbar(self._heatmap_plot_herb, ax=self._heatmap_herb_ax,
                               orientation='horizontal')

            self._heatmap_herb_ax.set_title("Herbivores density")

        if self._heatmap_plot_carn is not None:
            self._heatmap_plot_carn.set_data(carn_data)
        else:
            self._heatmap_plot_carn = self._heatmap_carn_ax.imshow(carn_data, cmap="Reds",
                                                                   interpolation='nearest',
                                                                   vmin=-0, vmax=c_max_carn)
            self._fig.colorbar(self._heatmap_plot_carn, ax=self._heatmap_carn_ax,
                               orientation='horizontal')

            self._heatmap_carn_ax.set_title("Carnivores density")

    def create_hists(self):
        """
        Create histogram plots.
        """

        if self._hist_fitness_ax is None:
            self._hist_fitness_ax.hist(self.animal_features_per_species['Herbivore']['Fitness'])
            self._hist_fitness_ax.hist(self.animal_features_per_species['Carnivore']['Fitness'])

        if self._hist_age_ax is None:
            self._hist_age_ax.hist(self.animal_features_per_species['Herbivore']['Age'])
            self._hist_age_ax.hist(self.animal_features_per_species['Carnivore']['Age'])

        if self._hist_weight_ax is None:
            self._hist_weight_ax.hist(self.animal_features_per_species['Herbivore']['Weight'])
            self._hist_weight_ax.hist(self.animal_features_per_species['Carnivore']['Weight'])

    def update_hists(self):
        """
        Recreate a histogram on the same ax.
        """
        fitness_herb = self.animal_features_per_species['Herbivore']['Fitness']
        fitness_carn = self.animal_features_per_species['Carnivore']['Fitness']
        age_herb = self.animal_features_per_species['Herbivore']['Age']
        age_carn = self.animal_features_per_species['Carnivore']['Age']
        weight_herb = self.animal_features_per_species['Herbivore']['Weight']
        weight_carn = self.animal_features_per_species['Carnivore']['Weight']

        if self._hist_specs:
            self._hist_fitness_ax.clear()

            f_max = self._hist_specs['fitness']['max']
            f_delta = self._hist_specs['fitness']['delta']
            _bins_fitness = int(f_max / f_delta)
            self._hist_fitness_ax.hist((fitness_herb, fitness_carn),
                                       _bins_fitness,
                                       (0, self._hist_specs['fitness']['max']), histtype='step',
                                       linewidth=2, label=('Herbivores', 'Carnivores'),
                                       color=('b', 'r'))

            self._hist_age_ax.clear()
            _bins_age = int(self._hist_specs['age']['max'] / self._hist_specs['age']['delta'])
            self._hist_age_ax.hist((age_herb, age_carn), _bins_age,
                                   (0, self._hist_specs['age']['max']), histtype='step',
                                   linewidth=2,
                                   label=('Herbivores', 'Carnivores'), color=('b', 'r'))

            self._hist_weight_ax.clear()

            w_max = self._hist_specs['weight']['max']
            w_delta = self._hist_specs['weight']['delta']
            _bins_weight = int(w_max / w_delta)
            self._hist_weight_ax.hist((weight_herb, weight_carn), _bins_weight,
                                      (0, self._hist_specs['weight']['max']), histtype='step',
                                      linewidth=2,
                                      label=('Herbivores', 'Carnivores'), color=('b', 'r'))

        else:
            self._hist_fitness_ax.clear()
            self._hist_fitness_ax.hist((fitness_herb, fitness_carn), 20,
                                       (0, 1), histtype='step',
                                       linewidth=2, label=('Herbivores', 'Carnivores'),
                                       color=('b', 'r'))

            self._hist_age_ax.clear()
            self._hist_age_ax.hist((age_herb, age_carn), 30,
                                   (0, 60), histtype='step',
                                   linewidth=2, label=('Herbivores', 'Carnivores'),
                                   color=('b', 'r'))
            self._hist_weight_ax.clear()
            self._hist_weight_ax.hist((weight_herb, weight_carn), 30,
                                      (0, 60), histtype='step',
                                      linewidth=2, label=('Herbivores', 'Carnivores'),
                                      color=('b', 'r'))

        self._hist_fitness_ax.set_title('Fitness', loc='center')
        self._hist_age_ax.set_title('Age', loc='center')
        self._hist_weight_ax.set_title('Weight', loc='center')

    def create_map(self):
        """
        Create the static map, in RGB colors.
        """
        #                   R    G    B
        rgb_value = {'W': (0.0, 0.0, 1.0),  # blue
                     'L': (0.0, 0.6, 0.0),  # dark green
                     'H': (0.5, 1.0, 0.5),  # light green
                     'D': (1.0, 1.0, 0.5)}  # light yellow

        map_rgb = [[rgb_value[column] for column in row]
                   for row in self.island.geogr.splitlines()]

        self._map_ax.imshow(map_rgb)
        self._map_ax.set_title('Island')

        ax_lg = self._fig.add_axes([0.40, 0.78, 0.2, 0.2])
        ax_lg.axis('off')
        for ix, name in enumerate(('Water', 'Lowland',
                                   'Highland', 'Desert')):
            ax_lg.add_patch(plt.Rectangle((0., ix * 0.2), 0.1, 0.1,
                                          edgecolor='none',
                                          facecolor=rgb_value[name[0]]))
            ax_lg.text(0.12, ix * 0.2, name, transform=ax_lg.transAxes)

    def create_animal_lines(self):
        """
        Create line graphs for the total count of each animal type
        """

        # Herbivore linegraph
        self._animal_line_ax.set_title('Animal count')
        if self._herbi_line_plot is None:
            linegraph_plot_herb = self._animal_line_ax.plot(np.arange(0, self.final_year + 1),
                                                            np.full(self.final_year + 1, np.nan))
            self._herbi_line_plot = linegraph_plot_herb[0]

            rgb_value = {'Herbivore': (0.0, 0.53, 1),  # blue
                         'Carnivore': (0.85, 0.53, 0)}  # orange (gul, rød)

            ax_lg = self._fig.add_axes([0.50, 0.85, 0.2, 0.2])
            ax_lg.axis('off')
            for ix, name in enumerate(('Carnivore', "Herbivore")):
                ax_lg.add_patch(plt.Rectangle((0., ix * 0.2), 0.1, 0.1,
                                              edgecolor='none',
                                              facecolor=rgb_value[name]))
                ax_lg.text(0.12, ix * 0.2, name, transform=ax_lg.transAxes)

        else:
            x_data, y_data = self._herbi_line_plot.get_data()
            x_new = np.arange(x_data[-1] + 1, self.final_year + 1)
            if len(x_new) > 0:
                y_new = np.full(x_new.shape, np.nan)
                self._herbi_line_plot.set_data(np.hstack((x_data, x_new)),
                                               np.hstack((y_data, y_new)))

        # Carnivore linegraph
        if self._carni_line_plot is None:
            linegraph_plot_carn = self._animal_line_ax.plot(np.arange(0, self.final_year + 1),
                                                            np.full(self.final_year + 1, np.nan))
            self._carni_line_plot = linegraph_plot_carn[0]

        else:
            x_data, y_data = self._carni_line_plot.get_data()
            x_new = np.arange(x_data[-1] + 1, self.final_year + 1)
            if len(x_new) > 0:
                y_new = np.full(x_new.shape, np.nan)
                self._carni_line_plot.set_data(np.hstack((x_data, x_new)),
                                               np.hstack((y_data, y_new)))

    def update_linegraph(self):
        """
        A method to continuously update the linegraph.

        Returns
        -----------
        Updated lineplot : plot
          Updated with new data
        """
        buffer = 20
        y_data = self._herbi_line_plot.get_ydata()
        y_data[self.years_simulated] = self.num_animals_per_species['Herbivore']
        self._herbi_line_plot.set_ydata(y_data)

        y_data = self._carni_line_plot.get_ydata()
        y_data[self.years_simulated] = self.num_animals_per_species['Carnivore']
        self._carni_line_plot.set_ydata(y_data)

        if self._ymax_animals:
            self._animal_line_ax.set_ylim(0, self._ymax_animals)
        else:
            curr_ylim = self._animal_line_ax.get_ylim()[1]
            new_max = max(self.num_animals_per_species["Herbivore"],
                          self.num_animals_per_species["Carnivore"])
            if curr_ylim + buffer < new_max:
                self._animal_line_ax.set_ylim(0, new_max + 20)

    def update_graphics(self):
        self.update_linegraph()
        self.update_hists()
        self.update_heatmaps()

        self._fig.canvas.flush_events()
        plt.suptitle(f"Simulation of year {self.year}", fontsize=10)

        plt.pause(1e-06)